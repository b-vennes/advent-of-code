use std::collections::HashMap;
use std::fs;

#[cfg(test)]
mod tests;

fn main() {
    calculate_shortest_manhattan_for_paths();
    calculate_shortest_step_distance_for_paths();
}

fn calculate_shortest_manhattan_for_paths() {
    let input = parse_input_to_vecs_str();

    let result = find_shortest_cross_manhattan_distance(input);

    println!("Shortest manhattan distance: {:?}", result);
}

fn calculate_shortest_step_distance_for_paths() {
    let input = parse_input_to_vecs_str();

    let result = find_shortest_cross_step_distance(input);

    println!("Shortest step distance: {:?}", result);
}

fn parse_input_to_vecs_str() -> Vec<Vec<String>> {
    let input_string = fs::read_to_string("input").expect("unable to read file");
    let path_strings: Vec<String> = input_string
        .split('\n')
        .map(|s| String::from(s))
        .collect();

    path_strings.iter().map(|path| {
        path
            .split(',')
            .map(|s| String::from(s))
            .collect()
    }).collect()
}

#[derive(Debug, Clone)]
struct WirePath {
    path_table: HashMap<String, bool>,
    x_pos: i32,
    y_pos: i32
}

impl WirePath {
    fn new(x_pos: i32, y_pos: i32) -> WirePath {
        WirePath {
            path_table: HashMap::new(),
            x_pos: x_pos,
            y_pos: y_pos
        }
    }

    fn perform_move(&mut self, move_code: &String) -> i32 {
        let (direction, num_steps) = WirePath::get_direction_and_amount_from_move_code(move_code);
    
        for _n in 1..num_steps+1 {
            self.step(direction);
    
            let next_space = format!("{},{}", self.x_pos, self.y_pos);
    
            self.path_table.insert(next_space, true);
        }

        num_steps
    }

    fn step(&mut self, direction: &str) {
        match direction {
            "U" => self.y_pos += 1,
            "D" => self.y_pos -= 1,
            "L" => self.x_pos -= 1,
            "R" => self.x_pos += 1,
            other => panic!("move code {} not implemented", other)
        };
    }

    fn follow_path(&mut self, path: Vec<String>) {
        path.iter().for_each(|step| {
            self.perform_move(step);
        });
    }

    fn get_direction_and_amount_from_move_code(move_code: &String) -> (&str, i32) {
        let direction = move_code.get(..1).unwrap();
        let num_steps: i32 = move_code.get(1..).unwrap()
            .parse().unwrap();

        (direction, num_steps)
    }
}

fn find_shortest_cross_manhattan_distance(paths: Vec<Vec<String>>) -> i32 {
    let mut wire_paths: Vec<WirePath> = Vec::new();

    paths.iter().for_each(|path| {
        let mut next_wire_path = WirePath::new(0, 0);

        next_wire_path.follow_path(path.to_vec());

        wire_paths.push(next_wire_path);
    });

    let crossings = find_wire_crossings(&wire_paths);

    find_shortest_crossing_manhattan_distance(&crossings)
}

fn find_shortest_cross_step_distance(paths: Vec<Vec<String>>) -> i32 {
    let mut wire_paths: Vec<WirePath> = Vec::new();

    paths.iter().for_each(|path| {
        let mut next_wire_path = WirePath::new(0, 0);

        next_wire_path.follow_path(path.to_vec());

        wire_paths.push(next_wire_path);
    });

    let crossings = find_wire_crossings(&wire_paths);

    find_shortest_steps_from_crossings(&crossings, &paths)
}

fn find_wire_crossings(wire_paths: &Vec<WirePath>) -> Vec<String> {
    let mut crosses: Vec<String> = Vec::new(); 
    
    for index in 0..wire_paths.len() {
        let wire_to_check = &wire_paths[index];

        let other_wires: Vec<WirePath> = wire_paths[index+1..].to_vec();

        for wire in other_wires {
            for space in wire.path_table.keys() {
                if wire_to_check.path_table.contains_key(space) && !crosses.contains(&space) {
                    crosses.push(space.clone())
                }
            }
        }
    }

    crosses
}

fn find_shortest_crossing_manhattan_distance(crossings: &Vec<String>) -> i32 {
    let mut shortest_distance: i32 = -1;
    
    crossings.iter().for_each(|crossing: &String| {
        let positions: Vec<&str> = crossing.split(',').collect();
        let x_pos: i32 = positions.get(0).unwrap().parse().unwrap();
        let y_pos: i32 = positions.get(1).unwrap().parse().unwrap();

        let distance = compute_manhattan_distance(x_pos, y_pos);

        if shortest_distance == -1 || distance < shortest_distance {
            shortest_distance = distance;
        }
    });

    shortest_distance
}

fn compute_manhattan_distance(x_pos: i32, y_pos: i32) -> i32 {
    let x_distance = x_pos.abs();
    let y_distance = y_pos.abs();
    
    x_distance + y_distance
}

fn find_shortest_steps_from_crossings(crossings: &Vec<String>, paths: &Vec<Vec<String>>) -> i32 {
    let mut shortest_distance = -1;
    
    crossings.iter().for_each(|crossing: &String| {
        let positions: Vec<&str> = crossing.split(',').collect();
        let x_pos: i32 = positions.get(0).unwrap().parse().unwrap();
        let y_pos: i32 = positions.get(1).unwrap().parse().unwrap();

        let distance = compute_steps_for_paths(x_pos, y_pos, paths);

        if shortest_distance == -1 || distance < shortest_distance {
            shortest_distance = distance;
        }
    });

    shortest_distance
}

fn compute_steps_for_paths(x_pos: i32, y_pos: i32, paths: &Vec<Vec<String>>) -> i32 {
    let mut total_steps: i32 = 0;

    for path in paths {
        let mut current_wire_path = WirePath::new(0, 0);
        let mut step_index = 0;

        while step_index < path.len() 
            && !(current_wire_path.x_pos == x_pos && current_wire_path.y_pos == y_pos) {
                let move_code = &path[step_index];
                let (direction, amount) = WirePath::get_direction_and_amount_from_move_code(&move_code);
                let mut step_count = 0;

                while step_count < amount 
                    && !(current_wire_path.x_pos == x_pos 
                    && current_wire_path.y_pos == y_pos) {
                        current_wire_path.step(direction);
                        step_count += 1;
                        total_steps += 1;
                }

                step_index += 1;
        }  
    }

    total_steps
}