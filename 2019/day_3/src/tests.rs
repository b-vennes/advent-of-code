use super::*;

#[test]
fn test_get_shortest_cross_distance_1() {
    let first_path = vec!["R75","D30","R83","U83","L12","D49","R71","U7","L72"]
        .iter()
        .map(|&s| String::from(s))
        .collect();
    let second_path = vec!["U62","R66","U55","R34","D71","R55","D58","R83"]
        .iter()
        .map(|&s| String::from(s))
        .collect();

    let result = find_shortest_cross_manhattan_distance(vec![first_path, second_path]);

    assert_eq!(result, 159);
}

#[test]
fn test_get_shortest_cross_distance_2() {
    let first_path = vec!["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"]
        .iter()
        .map(|&s| String::from(s))
        .collect();
    let second_path = vec!["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]
        .iter()
        .map(|&s| String::from(s))
        .collect();

    let result = find_shortest_cross_manhattan_distance(vec![first_path, second_path]);

    assert_eq!(result, 135);
}

#[test]
fn test_perform_move_short_distance_vertical() {
    let mut table = WirePath::new(0, 0);

    table.perform_move(&String::from("U3"));

    assert_eq!(table.path_table.contains_key(&String::from("0,1")), true);
    assert_eq!(table.path_table.contains_key(&String::from("0,2")), true);
    assert_eq!(table.path_table.contains_key(&String::from("0,3")), true);
}

#[test]
fn test_perform_move_short_distance_horizantal() {
    let mut table = WirePath::new(0, 0);

    table.perform_move(&String::from("L3"));

    assert_eq!(table.path_table.contains_key(&String::from("-1,0")), true);
    assert_eq!(table.path_table.contains_key(&String::from("-2,0")), true);
    assert_eq!(table.path_table.contains_key(&String::from("-3,0")), true);
}

#[test]
fn test_follow_path() {
    let path = vec!["U4", "R5", "D6"]
        .iter()
        .map(|&s| String::from(s))
        .collect();
    let mut wire_path = WirePath::new(0,0);

    wire_path.follow_path(path);

    assert_eq!(wire_path.path_table.contains_key("0,4"), true);
    assert_eq!(wire_path.path_table.contains_key("3,4"), true);
    assert_eq!(wire_path.path_table.contains_key("5,4"), true);
    assert_eq!(wire_path.path_table.contains_key("5,2"), true);
    assert_eq!(wire_path.path_table.contains_key("5,-1"), true);
}

#[test]
fn test_compute_manhattan_distance() {
    let location_x = 10;
    let location_y = 7;

    let distance_result = compute_manhattan_distance(location_x, location_y);

    assert_eq!(distance_result, 17);
}

#[test]
fn test_compute_manhattan_distance_negatives() {
    let location_x = 10;
    let location_y = -32;

    let distance_result = compute_manhattan_distance(location_x, location_y);

    assert_eq!(distance_result, 42);
}

#[test]
fn test_find_shortest_crossing() {
    let crossings = vec![String::from("4,5"), String::from("7,8"), String::from("-2,2")];

    let shortest = find_shortest_crossing_manhattan_distance(&crossings);

    assert_eq!(shortest, 4);
}

#[test]
fn test_find_wire_crossings() {
    let mut first_path = WirePath::new(0, 0);
    first_path.path_table = HashMap::new();
    first_path.path_table.insert(String::from("5,5"), true);
    first_path.path_table.insert(String::from("6,9"), true);
    first_path.path_table.insert(String::from("3,4"), true);
    first_path.path_table.insert(String::from("2,5"), true);

    let mut second_path = WirePath::new(0, 0);
    second_path.path_table = HashMap::new();
    second_path.path_table.insert(String::from("4,1"), true);
    second_path.path_table.insert(String::from("2,9"), true);
    second_path.path_table.insert(String::from("2,5"), true);
    second_path.path_table.insert(String::from("10,25"), true);

    let crossings = find_wire_crossings(&vec![first_path, second_path]);

    assert_eq!(crossings, vec!["2,5"]);
}

#[test]
fn test_find_shortest_cross_step_distance_1() {
    let path_1 = vec![String::from("R75"),String::from("D30"),String::from("R83"),String::from("U83"),String::from("L12"),String::from("D49"),String::from("R71"),String::from("U7"),String::from("L72")];

    let path_2 = vec![String::from("U62"),String::from("R66"),String::from("U55"),String::from("R34"),String::from("D71"),String::from("R55"),String::from("D58"),String::from("R83")];

    let result = find_shortest_cross_step_distance(vec![path_1, path_2]);

    assert_eq!(result, 610);
}

#[test]
fn test_find_shortest_cross_step_distance_2() {
    let path_1 = vec![String::from("R98"),String::from("U47"),String::from("R26"),String::from("D63"),String::from("R33"),String::from("U87"),String::from("L62"),String::from("D20"),String::from("R33"),String::from("U53"),String::from("R51")];

    let path_2 = vec![String::from("U98"),String::from("R91"),String::from("D20"),String::from("R16"),String::from("D67"),String::from("R40"),String::from("U7"),String::from("R15"),String::from("U6"),String::from("R7")];

    let result = find_shortest_cross_step_distance(vec![path_1, path_2]);

    assert_eq!(result, 410);
}

#[test]
fn test_compute_steps_for_paths_1() {
    let paths = vec![
        vec![String::from("U4"), String::from("R2"), String::from("U2")],
        vec![String::from("R1"), String::from("U7")]
    ];
    let x_crossing = 1;
    let y_crossing = 4;

    let steps = compute_steps_for_paths(x_crossing, y_crossing, &paths);

    assert_eq!(steps, 10);
}

#[test]
fn test_compute_steps_for_paths_2() {
    let paths = vec![
        vec![String::from("U1"), String::from("R1"), String::from("U1")],
        vec![String::from("R1"), String::from("U1")]
    ];
    let x_crossing = 1;
    let y_crossing = 1;

    let steps = compute_steps_for_paths(x_crossing, y_crossing, &paths);

    assert_eq!(steps, 4);
}