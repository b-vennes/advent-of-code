use super::*;

#[test]
fn test_valid_password_111122() {
    let password = 111122;

    let valid = is_valid_password(password);

    assert_eq!(valid, true);
}

#[test]
fn test_invalid_password_223450() {
    let password = 223450;

    let valid = is_valid_password(password);

    assert_eq!(valid, false);
}

#[test]
fn test_valid_password_112233() {
    let password = 112233;

    let valid = is_valid_password(112233);

    assert_eq!(valid, true);
}

#[test]
fn test_invalid_password_123444() {
    let password = 123444;

    let valid = is_valid_password(password);

    assert_eq!(valid, false);
}

#[test]
fn test_invalid_password_799999() {
    let password = 799999;

    let valid = is_valid_password(password);

    assert_eq!(valid, false);
}

#[test]
fn test_invalid_password_789999() {
    let password = 789999;

    let valid = is_valid_password(password);

    assert_eq!(valid, false);
}

#[test]
fn test_invalid_password_123789() {
    let password = 123789;

    let valid = is_valid_password(password);

    assert_eq!(valid, false);
}

#[test]
fn test_has_no_decreasing_values_valid() {
    let password = 124567;

    let valid = has_never_decreasing_digits(password);

    assert_eq!(valid, true);
}

#[test]
fn test_has_no_decreasing_values_invalid() {
    let password = 124527;

    let valid = has_never_decreasing_digits(password);

    assert_eq!(valid, false);
}

#[test]
fn test_has_matching_adjacent_digits_valid() {
    let password = 133456;

    let valid = has_matching_adjacent_digits(password);

    assert_eq!(valid, true);
}

#[test]
fn test_has_matching_adjacent_digits_invalid() {
    let password = 185473;

    let valid = has_matching_adjacent_digits(password);

    assert_eq!(valid, false);
}

#[test]
fn test_has_no_matching_groups_valid() {
    let password = 111122;

    let valid = has_matching_adjacent_digits(password);
    
    assert_eq!(valid, true);
}

#[test]
fn test_has_no_matching_groups_invalid() {
    let password = 122234;

    let valid = has_matching_adjacent_digits(password);
    
    assert_eq!(valid, false);
}

#[test]
fn test_concat_digits_1234() {
    let digits = vec![1,2,3,4];

    let result = concat_digits(digits);

    assert_eq!(result, 1234);
}

#[test]
fn test_concat_digits_6() {
    let digits = vec![6];

    let result = concat_digits(digits);

    assert_eq!(result, 6);
}

#[test]
fn has_paired_digits__contains_pair__returns_true() {
    let password = 1335555;

    let result = has_paired_digits(password);

    assert_eq!(result, true);
}

#[test]
fn has_paired_digits__no_pair__returns_false() {
    let password = 125555;

    let result = has_paired_digits(password);

    assert_eq!(result, false);
}