#[cfg(test)]
mod tests;

fn main() {
    let answer = get_number_of_matching_passwords(347312, 805915);
    println!("Number of matching passwords: {}", answer);
}

fn get_number_of_matching_passwords(lower_bound: i32, upper_bound: i32) -> usize {
    (lower_bound..upper_bound)
        .filter(|&password| is_valid_password(password))
        .count()
}

fn is_valid_password(password: i32) -> bool {
    if has_paired_digits(password)
    && has_never_decreasing_digits(password) {
        println!("{}", password);
        true
    } else {
        false
    }
}

/*
Used for first star.
*/
fn has_matching_adjacent_digits(password: i32) -> bool {
    let digits = get_digits(password);

    let mut i = 0;
    while i < digits.len() - 1 {
        if digits[i] == digits[i+1] {
            return true;
        } else {
            i += 1;
        }
    }

    false
}

fn has_paired_digits(password: i32) -> bool {
    let mut digits = get_digits(password);
    
    if digits.len() > 0 {
        let first = digits.remove(0);
        let mut i = 0;

        while digits.len() > 0 && digits[0] == first {
            digits.remove(0);
            i += 1;
        }

        if i == 1 {
            return true;
        } else {
            return has_paired_digits(concat_digits(digits));
        }
    }

    return false;
}

fn has_never_decreasing_digits(password: i32) -> bool {
    let digits = get_digits(password);

    let mut i = 0;
    while i < digits.len() - 1 {
        if digits[i] > digits[i+1] {
            return false;
        }
        i += 1;
    }

    true
}

fn get_digits(password: i32) -> Vec<i32> {
    let mut digits: Vec<i32> = vec![];
    let mut curr_value = password;

    while curr_value != 0 {
        digits.push(curr_value % 10);
        curr_value /= 10;
    }

    digits.reverse();

    digits
}

fn concat_digits(digits: Vec<i32>) -> i32 {
    let digits_size = digits.len() as u32;
    let mut count = 0 as u32;
    let mut result = 0;

    for d in digits {
        result += d * (10 as i32).pow(digits_size - count - 1);
        count += 1;
    }

    result
}
