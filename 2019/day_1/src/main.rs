use std::fs;

fn main() {
    let input = parse_input_to_vec_i32();
    let result: i32 = input.into_iter()
        .map(|mass| get_fuel_for_mass(mass) )
        .sum();
    
    println!("{}", result);
}

fn get_fuel_for_mass(mass: i32) -> i32 {
    match mass {
        num if num > 0 => {
            let fuel_required = (num / 3) - 2;
            if fuel_required > 0 {
                fuel_required + get_fuel_for_mass(fuel_required) 
            } else {
                0
            }
        },
        _ => 0
    }
}

fn parse_input_to_vec_i32() -> Vec<i32> {
    let input_string = fs::read_to_string("input").expect("unable to read file");
    let input_vector: Vec<&str> = input_string.split('\n').collect();
    input_vector.into_iter()
        .map(|s| s.parse().expect("Can't parse value"))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_fuel_test_large_mass() {
        assert_eq!(get_fuel_for_mass(1969), 966);
    }

    #[test]
    fn get_fuel_small_mass() {
        assert_eq!(get_fuel_for_mass(14), 2);
    }

    #[test]
    fn get_fuel_unsubstantial_mass() {
        assert_eq!(get_fuel_for_mass(5), 0);
    }
}
