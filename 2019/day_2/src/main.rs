use std::fs;
use std::io;

fn main() {
    let initial_memory = parse_input_to_vec_i32(',');

    for noun in 0..99 {
        for verb in 0..99 {
            let mut path_initial_memory = initial_memory.clone();
            path_initial_memory[1] = noun;
            path_initial_memory[2] = verb;

            let output = run_tape(path_initial_memory, 0);

            if output[0] == 19690720 {
                println!("noun {} -- verb {} -- solution {}", noun, verb, 100 * noun + verb);
            }
        }
    }
}

fn run_tape(start_tape: Vec<i32>, start_position: i32) -> Vec<i32> {
    let mut tape = start_tape;
    let mut position = start_position;

    while tape[position as usize] != 99 {
        tape = match step(tape, position) {
            Ok(t) => t,
            Err(e) => panic!(e)
        };
        position = position + 4;
    }

    tape
}

fn parse_input_to_vec_i32(seperator: char) -> Vec<i32> {
    let input_string = fs::read_to_string("input").expect("unable to read file");
    let input_vector: Vec<&str> = input_string.split(seperator).collect();
    input_vector.into_iter()
        .map(|s| s.parse().expect("Can't parse value"))
        .collect()
}

fn step(tape: Vec<i32>, position: i32) -> Result<Vec<i32>, io::Error> {
    let op_code = tape.get(position as usize);
    
    match op_code {
        Some(1) => perform_operation(tape, position, 1),
        Some(2) => perform_operation(tape, position, 2),
        Some(99) => Ok(tape),
        Some(other_op_code) => Err(io::Error::new(io::ErrorKind::Other, format!("Op-code {} is not valid", other_op_code))),
        None => Err(io::Error::new(io::ErrorKind::Other, format!("Position {} not found on tape", position)))
    }
}

fn perform_operation(mut tape: Vec<i32>, op_position: i32, operation: i32) -> Result<Vec<i32>, io::Error> {
    let position_1 = tape[(op_position + 1) as usize];
    let position_2 = tape[(op_position + 2) as usize];
    let output_position = tape[(op_position + 3) as usize];

    match operation {
        1 => {
            tape[output_position as usize] = tape[position_1 as usize] + tape[position_2 as usize];
            Ok(tape)
        },
        2 => {
            tape[output_position as usize] = tape[position_1 as usize] * tape[position_2 as usize];
            Ok(tape)
        }, 
        unknown_op => Err(io::Error::new(io::ErrorKind::Other, format!("Operation {} is not known", unknown_op)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_step_add() {
        let test_tape = vec![1,0,0,0];

        let updated_tape = step(test_tape, 0).unwrap();

        assert_eq!(updated_tape, vec![2,0,0,0]);
    }

    #[test]
    fn test_step_multiply() {
        let test_tape = vec![2,3,0,3];

        let updated_tape = step(test_tape, 0).unwrap();

        assert_eq!(updated_tape, vec![2,3,0,6]);
    }

    #[test]
    fn test_step_bad_opcode() {
        let test_tape = vec![45,1,0,0];

        let result = step(test_tape, 0);

        match result {
            Ok(_) => assert!(false, "op code should not have been valid"),
            Err(_) => assert!(true)
        }
    }

    #[test]
    fn test_step_bad_position() {
        let test_tape = vec![1,1,0,0];
        let position = 20;

        let result = step(test_tape, position);

        match result {
            Ok(_) => assert!(false, "position should not have been valid"),
            Err(_) => assert!(true)
        }
    }

    #[test]
    fn test_run_tape_success() {
        let test_tape = vec![1,1,1,4,99,5,6,0,99];

        let result = run_tape(test_tape, 0);

        assert_eq!(result, vec![30,1,1,4,2,5,6,0,99]);
    }
}